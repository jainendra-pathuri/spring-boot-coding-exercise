# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to know test user record api

  Scenario: Success response
    Given url 'http://localhost:8080'
    And path '/user/old'
    And param recCount = 2
    When method GET
    Then status 200
    And match response == '#[2]'
    
 
  Scenario: Error response
    Given url 'http://localhost:8080'
    And path '/user/old'
    And param recCount = -2
    When method GET
    Then status 400
    And match response == 
    """
    { 
      status : '#number',
      errorMessage : '#string',
      timestamp: '#string',
      trace: '#string'
    }
    """