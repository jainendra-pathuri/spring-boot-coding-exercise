package com.telstra.codechallenge.user.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.telstra.codechallenge.user.dto.UserAccount;

@Service
public class UserService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${user.search.repo.url}")
	private String userSearchRepoUrl;
	
	private static String ITEMS = "items";
	private static String ID = "id";
	private static String HTMLURL = "html_url";
	private static String OWNER = "owner";
	private static String LOGIN = "login";
	
	private static String SEARCH_QUERY = "q";
	private static String SORT = "sort";
	private static String ORDER = "order";
	private static String PER_PAGE = "per_page";
	
	public List<UserAccount> getOldZeroFollowRecord(String recCount) throws Exception{
		List<UserAccount> lstUserAccount = new ArrayList<UserAccount>();
		URI url = buildZeroFollowerURL(recCount);
		//Trigger rest end point
		ResponseEntity<JsonNode> response = restTemplate.exchange(url, HttpMethod.GET,null,JsonNode.class);
		JsonNode resObj = response.getBody();
		//Prepare response object
		if(resObj.get(ITEMS) != null && resObj.get(ITEMS).isArray()) {
			for(JsonNode item: resObj.get(ITEMS)) {
				UserAccount userAccount = new UserAccount();
				userAccount.setId(isNotBlank(item.get(ID)) ? 
						item.get(ID).asText() : null);
				userAccount.setHtml_url(isNotBlank(item.get(HTMLURL)) ? 
						item.get(HTMLURL).asText() : null);
				userAccount.setLogin(isNotBlank(item.get(OWNER)) ? 
						isNotBlank(item.get(OWNER).get(LOGIN)) ? 
								item.get(OWNER).get(LOGIN).asText() : null : null);
				lstUserAccount.add(userAccount);
			}
		}
		return lstUserAccount;
	}
	
	//Prepare url to be triggered
	private URI buildZeroFollowerURL(String recCount) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(userSearchRepoUrl)
				.queryParam(SEARCH_QUERY, "followers=0")
				.queryParam(SORT, "joined")
				.queryParam(ORDER, "asc")
				.queryParam(PER_PAGE, recCount);
		
		return uriBuilder.build().toUri();
	}
	
	//Validate if node exist and also the value is not null
	private boolean isNotBlank(JsonNode node) {
		return node != null && node.asText() != null;
	}
}
