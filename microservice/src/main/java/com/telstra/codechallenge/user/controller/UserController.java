package com.telstra.codechallenge.user.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.telstra.codechallenge.user.dto.UserAccount;
import com.telstra.codechallenge.user.service.UserService;
import com.telstra.codechallenge.user.util.UserUtils;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	private static String TIMESTAMP = "timestamp";
	private static String STATUS = "status";
	private static String DATA = "data";
	private static String ERRORMESSAGE = "errorMessage";
	private static String TRACE = "trace";
	
	@GetMapping("/user/old")
	public ResponseEntity getOldRecord(@RequestParam(required = true) String recCount) throws Exception {
		Map resultBody = null;
		try {
			//Validate the request
			if(UserUtils.validateReqParam(recCount)) {
				List<UserAccount> result = userService.getOldZeroFollowRecord(recCount);
				//Send the results.
				return ResponseEntity.status(HttpStatus.OK).body(result);
			}
		} catch(ValidationException exp) {
			resultBody = prepareErrorObj(exp, HttpStatus.BAD_REQUEST.value());
		} catch(HttpClientErrorException exp) {
			resultBody = prepareErrorObj(exp, exp.getStatusCode().value());
		} catch(Exception exp) {
			resultBody = prepareErrorObj(exp, HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return ResponseEntity.status((int)resultBody.get(STATUS)).body(resultBody);
		
	}
	//Prepare error response of the API	
	private Map prepareErrorObj(Exception exp, Integer statusCode) {
		return Map.of(
				TIMESTAMP,new Date(),
				STATUS,statusCode.intValue(),
				ERRORMESSAGE,ExceptionUtils.getMessage(exp),
				TRACE,ExceptionUtils.getStackTrace(exp)
		);
	}	
}
