package com.telstra.codechallenge.user.util;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;

public class UserUtils {

	public static Boolean validateReqParam(String recCount) throws Exception {
		//Only allow positive numbers eg. 1,2,22.. only. Not -2,2.3,2f,abc..
		if(!StringUtils.isNumeric(recCount)) {
			throw new ValidationException("Invalid/No recCount received");
		}
		return Boolean.TRUE;
	} 
}
