package com.telstra.codechallenge.user.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telstra.codechallenge.user.dto.UserAccount;

public class UserServiceTest {
	
	@InjectMocks
	private UserService userService;
	
	@Mock
	private RestTemplate restTemplateMock;
	
	JsonNode jsonNodeMock;
	ResponseEntity<JsonNode> responseEntityMock;
	
	@BeforeEach
	public void setup() throws JsonMappingException, JsonProcessingException {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(userService, "userSearchRepoUrl", "http://forTestCase.com");
	}
	
	@Test
	public void getOldZeroFollowRecord() {
		Exception exp = null;
		List<UserAccount> lstUserAccount = null;
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = "{\"items\": [{\"id\": \"1\",\"html_url\": \"testURL\",\"owner\": {\"login\": \"testUser1\"}}]}";
			jsonNodeMock = mapper.readTree(jsonString);
			responseEntityMock = new ResponseEntity<JsonNode>(jsonNodeMock,HttpStatus.OK);
			
			when(restTemplateMock.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(),
					ArgumentMatchers.<Class<JsonNode>>any())).thenReturn(responseEntityMock);

			lstUserAccount = userService.getOldZeroFollowRecord("3");

		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals(1,lstUserAccount.size());
	}
	
	@Test
	public void getOldZeroFollowRecordException() {
		Exception exp = null;
		List<UserAccount> lstUserAccount = null;
		try {
			when(restTemplateMock.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(),
					ArgumentMatchers.<Class<JsonNode>>any())).thenThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR,"Request failed"));

			lstUserAccount = userService.getOldZeroFollowRecord("3");

		} catch (Exception e) {
			exp = e;
		}
		assertNotNull(exp);
		assertNull(lstUserAccount);
		assertEquals("500 Request failed",exp.getMessage());
	}
	
	@Test
	public void getOldZeroFollowEmptyRecord() {
		Exception exp = null;
		List<UserAccount> lstUserAccount = null;
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = "{\"id\": \"2\"}";
			jsonNodeMock = mapper.readTree(jsonString);
			responseEntityMock = new ResponseEntity<JsonNode>(jsonNodeMock,HttpStatus.OK);
			
			when(restTemplateMock.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(),
					ArgumentMatchers.<Class<JsonNode>>any())).thenReturn(responseEntityMock);

			lstUserAccount = userService.getOldZeroFollowRecord("3");

		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals(0,lstUserAccount.size());
	}
	
	@Test
	public void getOldZeroFollowItemEmptyRecord() {
		Exception exp = null;
		List<UserAccount> lstUserAccount = null;
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = "{\"items\": []}";
			jsonNodeMock = mapper.readTree(jsonString);
			responseEntityMock = new ResponseEntity<JsonNode>(jsonNodeMock,HttpStatus.OK);
			
			when(restTemplateMock.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(),
					ArgumentMatchers.<Class<JsonNode>>any())).thenReturn(responseEntityMock);

			lstUserAccount = userService.getOldZeroFollowRecord("3");

		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals(0,lstUserAccount.size());
	}
	
	@Test
	public void getOldZeroFollowNoHtmlRecord() {
		Exception exp = null;
		List<UserAccount> lstUserAccount = null;
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = "{\"items\": [{\"id\": \"1\",\"owner\": {\"login\": \"testUser1\"}}]}";
			jsonNodeMock = mapper.readTree(jsonString);
			responseEntityMock = new ResponseEntity<JsonNode>(jsonNodeMock,HttpStatus.OK);
			
			when(restTemplateMock.exchange(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(),
					ArgumentMatchers.<Class<JsonNode>>any())).thenReturn(responseEntityMock);

			lstUserAccount = userService.getOldZeroFollowRecord("3");

		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals(1,lstUserAccount.size());
		assertNull(lstUserAccount.get(0).getHtml_url());
	}

}
