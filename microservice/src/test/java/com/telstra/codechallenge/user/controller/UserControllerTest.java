package com.telstra.codechallenge.user.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.telstra.codechallenge.user.dto.UserAccount;
import com.telstra.codechallenge.user.service.UserService;

@SuppressWarnings("rawtypes")
public class UserControllerTest {

	@Mock
	UserService userServiceMock;
	
	@InjectMocks
	UserController userController;
	
	String recCount = "10";
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetOldRecordWithNullCount() {
		Exception exp = null;
		Map respObj = null;
		try {
			ResponseEntity response = userController.getOldRecord(null);
			respObj = (Map) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals("ValidationException: Invalid/No recCount received", respObj.get("errorMessage"));
	}
	
	@Test
	public void testGetOldRecordWithNotNumberCount() {
		Exception exp = null;
		Map respObj = null;
		try {
			ResponseEntity response = userController.getOldRecord("abc");
			respObj = (Map) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		
		assertEquals("ValidationException: Invalid/No recCount received", respObj.get("errorMessage"));
	}
	
	@Test
	public void testGetOldRecordWithNegativeCount() {
		Exception exp = null;
		Map respObj = null;
		try {
			ResponseEntity response = userController.getOldRecord("-256");
			respObj = (Map) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals("ValidationException: Invalid/No recCount received", respObj.get("errorMessage"));
	}
	
	@Test
	public void testGetOldRecordException() {
		Exception exp = null;
		Map respObj = null;
		try {
			when(userServiceMock.getOldZeroFollowRecord(recCount)).thenThrow(new Exception("Rest client error"));
			ResponseEntity response = userController.getOldRecord(recCount);
			respObj = (Map) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertEquals("Exception: Rest client error", respObj.get("errorMessage"));
	}
	
	@Test
	public void testGetOldRecord() {
		Exception exp = null;
		List<UserAccount> respObj = null;
		try {
			List<UserAccount> lstUsrAccountMock = new ArrayList<UserAccount>();
		
			UserAccount usrAccObj1 = new UserAccount();
			usrAccObj1.setId("1");usrAccObj1.setHtml_url("http://test1.com");usrAccObj1.setLogin("usr1");
			UserAccount usrAccObj2 = new UserAccount();
			usrAccObj2.setId("2");usrAccObj2.setHtml_url("http://test2.com");usrAccObj2.setLogin("usr2");
			UserAccount usrAccObj3 = new UserAccount();
			usrAccObj3.setId("3");usrAccObj3.setHtml_url("http://test3.com");usrAccObj3.setLogin("usr3");
			
			lstUsrAccountMock.add(usrAccObj1);lstUsrAccountMock.add(usrAccObj2);lstUsrAccountMock.add(usrAccObj3);
			when(userServiceMock.getOldZeroFollowRecord(recCount)).thenReturn(lstUsrAccountMock);
			
			ResponseEntity response = userController.getOldRecord(recCount);
			respObj = (List<UserAccount>) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertNotNull(respObj);
		
		assertEquals(3,((List<UserAccount>)respObj).size());
	}
	
	@Test
	public void testGetOldEmptyRecord() {
		Exception exp = null;
		List<UserAccount> respObj = null;
		try {
			List<UserAccount> lstUsrAccountMock = new ArrayList<UserAccount>();
			when(userServiceMock.getOldZeroFollowRecord(recCount)).thenReturn(lstUsrAccountMock);
			
			ResponseEntity response = userController.getOldRecord(recCount);
			respObj = (List<UserAccount>) response.getBody();
		} catch (Exception e) {
			exp = e;
		}
		assertNull(exp);
		assertNotNull(respObj);
		assertEquals(0,((List<UserAccount>)respObj).size());
	}
}
